/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h 
 * @brief Header file for stats.c source code
 *
 * <Add Extended Description Here>
 *
 * @author Benjamin Barbosa
 * @date 11/11/2019
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/* Function Prototypes */ 

/**
 * @brief A function that prints the statics of an array
 *
 * Uses another functions to print statics of an array given.
 * This includes: minimum, maximum, mean and median.
 *
 * @param None
 *
 * @return None
 */
void print_statics(unsigned char arr[]);

/**
 * @brief Prints the array to the screen
 *
 * Given and array of data and length, prints the array to the screen
 *
 * @param ptr Pointer to the array given
 * @param size Size of the array given
 *
 * @return None
 */
void print_array( unsigned char arr[], int size );

/**
 * @brief Finds median value of and array
 *
 * Given and array of data and length, returns the median value of an array
 *
 * @param ptr Pointer to the array given
 * @param size Size of the array given
 *
 * @return The median value of the array
 */
float find_median( unsigned char arr[], int size );

/**
 * @brief Finds mean value of and array
 *
 * Given an array of data and a length, returns the mean
 *
 * @param ptr Pointer to the array given
 * @param size Size of the array given
 *
 * @return The mean value of the array
 */
float find_mean( unsigned char arr[], int size );

/**
 * @brief Finds maximum value of and array
 * 
 * Given an array of data and a length, returns the maximum
 *
 * @param ptr Pointer to the array given
 * @param size Size of the array given
 *
 * @return The maximum value of the array
 */
unsigned char find_maximum( unsigned char arr[], int size );

/**
 * @brief Finds minimum value of and array
 *
 * Given an array of data and a length, returns the minimum
 *
 * @param ptr Pointer to the array given
 * @param size Size of the array given
 *
 * @return The minimum value of the array
 */
unsigned char find_minimum( unsigned char arr[], int size );

/**
 * @brief Sorts a given array
 *
 * Given an array of data and a length, sorts the array from largest to smallest. 
 * (The zeroth Element should be the largest value, and the last element (n-1) 
 * should be the smallest value. )
 *
 * @param ptr Pointer to the array given
 * @param size Size of the array given
 *
 * @return None
 */
void sort_array( unsigned char arr[], int size );

#endif /* __STATS_H__ */
