/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file <Add File Name> 
 * @brief <Add Brief Description Here >
 *
 * <Add Extended Description Here>
 *
 * @author <Add FirsName LastName>
 * @date <Add date >
 *
 */



#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  /* Other Variable Declarations Go Here */
  /* Statistics and Printing Functions Go Here */
  print_statics(test);  
}

/******************************************************************************/
  /* FUNCTION DEFINITIONS */
/******************************************************************************/

void print_statics(unsigned char arr[]){
  printf("\n\nThe array is: ");
  print_array( arr , SIZE );
  printf("\n\nThe sorted array is: ");
  sort_array( arr , SIZE );
  print_array( arr , SIZE );
  printf("\n\nMinimum from array is: %d \n\n",   find_minimum( arr , SIZE ));
  printf("Maximum from array is: %d \n\n",   find_maximum( arr , SIZE ));
  printf("Mean from array is: %f \n\n",   find_mean( arr , SIZE ));
  printf("Median from array is: %f \n\n",   find_median( arr , SIZE ));
}

void print_array( unsigned char arr[], int size ){

  printf("\n test [] = {");

  for ( int i = 0 ; i < size  ; i++ ){

    if ( i == size-1 ){
      printf( " %d ", arr[i] );
    }

    else{
      printf( " %d, ", arr[i] );
    }

  }
  printf("} \n"); 

}
 
float find_median( unsigned char arr[], int size ){

  float median;
  sort_array( arr , SIZE );

  if( SIZE % 2 != 0 ){
    median = arr[ ( SIZE - 1 ) / 2 ];
  }
  else{   
    median = ( (float)arr[ SIZE/2 ] + (float)arr[ (SIZE/2) - 1] ) / 2;
  }
  return median;
}


float find_mean( unsigned char arr[], int size ){
  float sum = 0;

  for(int i = 0 ; i < size ; i++){
    sum += (float)arr[i];
  }

  return sum / SIZE;
}

unsigned char find_maximum( unsigned char arr[], int size ){
  sort_array( arr , SIZE );
  return arr[0];  
}

unsigned char find_minimum( unsigned char arr[], int size ){
  sort_array( arr , SIZE );
  return arr[SIZE - 1];
}

void sort_array( unsigned char arr[], int size ){
  int i, j;
  for (j = 0 ; j < size ; j++){
    for(i=  0; i < size - 1 ; i++){
      if(arr[i] < arr[i+1]){
        int temp;
        temp = arr[i+1];
        arr[i+1] = arr [i];
        arr[i] = temp;
      }
    }
  }      
}
