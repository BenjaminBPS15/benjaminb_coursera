	.cpu cortex-m4
	.eabi_attribute 27, 1
	.eabi_attribute 28, 1
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 6
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.file	"main.c"
	.comm	buffer,10,4
	.text
	.align	1
	.global	main
	.syntax unified
	.thumb
	.thumb_func
	.fpu fpv4-sp-d16
	.type	main, %function
main:
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 1, uses_anonymous_args = 0
	push	{r7, lr}
	sub	sp, sp, #8
	add	r7, sp, #0
	movs	r1, #10
	ldr	r0, .L5
	bl	clear_all
	ldr	r3, .L5+4
	movs	r2, #2
	movs	r1, #43
	mov	r0, r3
	bl	set_all
	movs	r2, #97
	movs	r1, #0
	ldr	r0, .L5
	bl	set_value
	movs	r1, #9
	ldr	r0, .L5
	bl	get_value
	mov	r3, r0
	strb	r3, [r7, #3]
	ldrb	r3, [r7, #3]
	adds	r3, r3, #39
	uxtb	r3, r3
	mov	r2, r3
	movs	r1, #9
	ldr	r0, .L5
	bl	set_value
	movs	r2, #55
	movs	r1, #3
	ldr	r0, .L5
	bl	set_value
	movs	r2, #88
	movs	r1, #1
	ldr	r0, .L5
	bl	set_value
	movs	r2, #50
	movs	r1, #4
	ldr	r0, .L5
	bl	set_value
	movs	r1, #1
	ldr	r0, .L5
	bl	get_value
	mov	r3, r0
	strb	r3, [r7, #3]
	movs	r2, #121
	movs	r1, #2
	ldr	r0, .L5
	bl	set_value
	ldrb	r3, [r7, #3]
	subs	r3, r3, #12
	uxtb	r3, r3
	mov	r2, r3
	movs	r1, #7
	ldr	r0, .L5
	bl	set_value
	movs	r2, #95
	movs	r1, #5
	ldr	r0, .L5
	bl	set_value
	movs	r3, #0
	str	r3, [r7, #4]
	b	.L2
.L3:
	ldr	r3, [r7, #4]
	adds	r3, r3, #1
	str	r3, [r7, #4]
.L2:
	ldr	r3, [r7, #4]
	cmp	r3, #9
	bls	.L3
	movs	r3, #0
	mov	r0, r3
	adds	r7, r7, #8
	mov	sp, r7
	@ sp needed
	pop	{r7, pc}
.L6:
	.align	2
.L5:
	.word	buffer
	.word	buffer+8
	.size	main, .-main
	.ident	"GCC: (15:6.3.1+svn253039-1build1) 6.3.1 20170620"
